import React from 'react';
import { Row, Col } from 'react-bootstrap';
import Link from 'next/link'

const Header = ()=>{
    return(
        <div className="headcss">
            <Row>
                <Col sm="1" style={{marginLeft:"5%", marginTop:"5px"}}>
                <div className="logohead" style={{height:"50px",width:"50px"}}></div>
                </Col>
                <Col sm="1" style={{marginTop:"30px"}}>
                <Link href="/">
                <text style={{color:"white",alignself:"center"}}>Home</text>
                </Link>
                </Col>
                <Col sm="1"style={{marginTop:"30px"}}>
                <Link href="/">
                <text style={{color:"white",alignself:"center",marginTop:"30px"}}>About us</text>
                </Link>
                </Col>
                <Col sm="1"style={{marginTop:"30px"}}>
                <Link href="/">
                <text style={{color:"white" ,alignself:"center",marginTop:"30px"}}>Contact us</text>
                </Link>
                </Col>
            </Row>
        </div>
    )
}
export default Header