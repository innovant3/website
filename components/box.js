import React from 'react';
import { Row, Col } from 'react-bootstrap';
import Link from 'next/link'
import Image from 'next/image'

const Box= ()=>{
    return(
        <div className="box">
            <Row>
            <Link href="/register">
                <Col sm="5" style={{marginLeft:"20px", marginTop:"0px",height:"150px",borderRight:"1px solid black",alignContent:"center"}}>
                    <div style={{marginTop:"20px"}}>
                    <Image src="/user.png" height={40} width={40} />
                    <h5 style={{text:"bold",color:"black"}}>Register</h5>
                    <p style={{color:"grey",fontSize:"10px"}}>Browse and find what you need.</p>
                    </div>
                </Col>
            </Link>
            <Link href="/login">

                <Col sm="5" style={{marginTop:"20px", marginLeft:"20px",alignContent:"center"}}>
                <Image src="/log.png" height={40} width={40} />
                <h5 style={{text:"bold",color:"black"}}>Sign in</h5>
                    <p style={{color:"grey",fontSize:"10px",overflow:"hidden",whiteSpace:"nowrap"}}>Already have an account,then welcome back.</p>
                </Col>
            </Link>

               
            </Row>
        </div>
    )
}
export default Box