
import React from 'react';
import { Form, Button, Alert, Row, Col } from 'react-bootstrap';
import {useFormik} from "formik";
import { signin } from '../services/Auth.service';
import "bootstrap/dist/css/bootstrap.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../components/header';
import Footer from '../components/footer';
import Box from '../components/box';
import Link from 'next/link'
import Image from 'next/image'
import Router from 'next/router' 

const Login = () => {

    const initialValues = {
        email: "",
        password: "",
    
    }

    
    
    function onSubmit(values) {
        const registered = {
            email: values.email,
            password: values.password
        };
        console.log(registered);
        signin(registered);
     
    }

    const validate = values => {
        const errors = {};
        if (!values.email) {
            errors.email = 'email is required';
        }
       

        if (!values.password) {
            errors.password = 'password is required';
        } else {
            if (values.password.length < 6) {
                errors.password = 'minimum length of the password is 6'
            }
        }
       
       
        return errors;
    }

    const formik = useFormik({ initialValues, onSubmit, validate });
    return (
        <div  >
       
        <div className="head">
            
                <Link href="/register" >
                <text style={{color:"grey",position:'absolute',marginLeft:"95%"}}>Register</text>
                </Link>
                <Link href="/login">

                <text style={{color:"grey",position:'absolute',marginLeft:"90%"}}>Sign in</text>
                </Link>

            
        </div>
        <div>
            <Header></Header>
        </div>
        <div style={{ backgroundColor: "gainsboro",height:"1000px"}}>

        <Row >
        <Col sm={3}></Col>
        <Col sm={6} >
        
        <div style ={{alignItems: "center" ,marginTop:"20%",marginLeft:"10%"}}>
            <div className="check">
            <Image src="/check.png" height={30} width={30} />
            </div>
            <Box ></Box>
            <Form style={{ width: "100%"}} onSubmit={formik.handleSubmit}>
                <Form.Group style={{paddingTop:"5%"}}>
                    <Form.Control
                        name="email"
                        className={(formik.touched.email && formik.errors.email) ? 'form-control is-invalid' : 'form-control'}
                        type="email" value={formik.values.email}
                        onChange={formik.handleChange}
                        placeholder="Email*" />
                         {formik.touched.email && formik.errors.email ? (
                                    <div className="invalid-feedback">{formik.errors.email}</div>
                                ) : null}
                </Form.Group>
                <br />  
                <Form.Group >
                    <Form.Control name="password"
                        className={(formik.touched.password && formik.errors.password) ? 'form-control is-invalid' : 'form-control'}
                        type="password" placeholder="Password*"
                        value={formik.values.password}
                        onChange={formik.handleChange} />
                          {formik.touched.password && formik.errors.password ? (
                                    <div className="invalid-feedback">{formik.errors.password}</div>
                                ) : null}
                </Form.Group>
                <br />
                
                <Button style={{background:"black",width:"20%", marginLeft:"40%",color:"grey"}} onClick={formik.validateForm} type="submit" >Submit</Button>
                
            </Form>
        </div>
        </Col>
                
        </Row>

        </div>
        <div>
                <Footer></Footer>
            </div>
        </div>
    )
}

export default Login;