import React from 'react';
import "bootstrap/dist/css/bootstrap.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from '../components/header';
import Footer from '../components/footer';
import Box from '../components/box';
import Image from 'next/image'
import { Form, Button, Alert, Row, Col } from 'react-bootstrap';

import { useFormik } from "formik";

import { register } from '../services/Auth.service';

import Link from 'next/link';

const Register = () => {


    const initialValues = {
        FirstName: "",
        LastName: "",
        email: "",
        password: "",
        confirmpassowrd: "",
    }
    function onSubmit(values) {

        const registered = {
            FirstName: values.FirstName,
            LastName: values.LastName,
            email: values.email,
            password: values.password
        };
        console.log(registered);
        register(registered);
    }
    const validate = values => {
        const errors = {};
        if (!values.FirstName) {
            errors.FirstName = 'first name is required';
        }

        if (!values.LastName) {
            errors.LastName = 'last name is required';
        }

        if (!values.email) {
            errors.email = 'email is required';
        }


        if (!values.password) {
            errors.password = 'password is required';
        } else {
            if (values.password.length < 6) {
                errors.password = 'minimum length of the password is 6'
            }
        }
        if (values.confirmpassword !== values.password) {
            errors.confirmpassword = 'passwords do not match ';
        }

        return errors;
    }

    const formik = useFormik({ initialValues, onSubmit, validate });
    return (
        <div  >
       
            <div className="head">
                
            <Link href="/register" >
                    <text style={{color:"grey",position:'absolute',marginLeft:"95%"}}>Register</text>
             </Link>
             <Link href="/login">  
                    <text style={{color:"grey",position:'absolute',marginLeft:"90%"}}>Sign in</text>
                    </Link>     

                
            </div>
            <div>
                <Header></Header>
            </div>
            <div style={{ backgroundColor: "gainsboro", height:"1000px"}}>

                <Row >
                    <Col sm={3}></Col>
                    <Col sm={6} >
                    <div style ={{alignItems: "center" ,marginTop:"20%",marginLeft:"10%"}}>
             <div className="checkr">
            <Image src="/check.png" height={30} width={30} />
            </div>
            <Box ></Box>
                            <Form style={{ width: "100%" }} onSubmit={formik.handleSubmit}>
                                <div>
                                    <Row>
                                        <Col sm={6}>
                                            <Form.Group style={{ paddingTop: "5%" }}>
                                                <Form.Control
                                                    name="FirstName"
                                                    className={(formik.touched.FirstName && formik.errors.FirstName) ? 'form-control is-invalid' : 'form-control'}
                                                    type="text" value={formik.values.FirstName}
                                                    onChange={formik.handleChange}
                                                    placeholder="First Name*" />
                                                {formik.touched.FirstName && formik.errors.FirstName ? (
                                                    <div className="invalid-feedback">{formik.errors.FirstName}</div>
                                                ) : null}
                                            </Form.Group>
                                        </Col>
                                        <Col sm={6}>
                                            <Form.Group style={{ paddingTop: "5%" }}>
                                                <Form.Control
                                                    name="LastName"
                                                    className={(formik.touched.LastName && formik.errors.LastName) ? 'form-control is-invalid' : 'form-control'}
                                                    type="text" value={formik.values.LastName}
                                                    onChange={formik.handleChange}
                                                    placeholder="Last Name*" />
                                                {formik.touched.LastName && formik.errors.LastName ? (
                                                    <div className="invalid-feedback">{formik.errors.lname}</div>
                                                ) : null}
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                </div>
                                <Form.Group style={{ paddingTop: "5%" }}>
                                    <Form.Control
                                        name="email"
                                        className={(formik.touched.email && formik.errors.email) ? 'form-control is-invalid' : 'form-control'}
                                        type="email" value={formik.values.email}
                                        onChange={formik.handleChange}
                                        placeholder="Email*" />
                                    {formik.touched.email && formik.errors.email ? (
                                        <div className="invalid-feedback">{formik.errors.email}</div>
                                    ) : null}
                                </Form.Group>
                                <br />
                                <Form.Group >
                                    <Form.Control name="password"
                                        className={(formik.touched.password && formik.errors.password) ? 'form-control is-invalid' : 'form-control'}
                                        type="password" placeholder="Password*"
                                        value={formik.values.password}
                                        onChange={formik.handleChange} />
                                    {formik.touched.password && formik.errors.password ? (
                                        <div className="invalid-feedback">{formik.errors.password}</div>
                                    ) : null}
                                </Form.Group>
                                <br />
                                <Form.Group >
                                    <Form.Control name="confirmpassword"
                                        className={(formik.touched.confirmpassword && formik.errors.confirmpassword) ? 'form-control is-invalid' : 'form-control'}
                                        type="password" placeholder="Repeat Password*"
                                        value={formik.values.confirmpassword}
                                        onChange={formik.handleChange} />
                                    {formik.touched.confirmpassword && formik.errors.confirmpassword ? (
                                        <div className="invalid-feedback">{formik.errors.confirmpassword}</div>
                                    ) : null}
                                </Form.Group>
                                <br />
                                <Button style={{ background: "black", width: "20%", marginLeft: "40%", marginBottom: "3%",color:"grey" }} type="submit" onClick={formik.validateForm}>Submit</Button>

                            </Form>
                        </div>
                    </Col>

                </Row>

            </div>
            <div>
                <Footer></Footer>
            </div>
        </div>
    )
}

export default Register;